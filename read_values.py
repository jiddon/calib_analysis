import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import json
import os

DEBUG = False
# DEBUG = True
ONLY_NEW_FMT = True
DIRECTORY = "./data/"

print(f"DEBUG: {DEBUG}, ONLY_NEW_FMT: {ONLY_NEW_FMT}, DIRECTORY: {DIRECTORY}")

def read_mapping():
    mapping = pd.read_csv("./mapping.csv", sep='\s*,\s*', engine='python')
    return mapping

MAP = read_mapping()

def ReadDbFile(name):
    mydict = dict()
    iov = None
    with open(name) as fp:
        lines = fp.readlines()
        for n,line in enumerate(lines):
            if(line.startswith('[\'')):
                line = line.rstrip(']\n')
                arrayline = line.split(' : ')
                mydict = json.loads(arrayline[1])
            else:
                iov = line[:line.find(" - ")]
                iov = iov.strip('][').split(',')
                iov = [int(i) for i in iov]
    return mydict, iov

def fill_dict(directory=DIRECTORY):
    """Fill a dictionary with one dataframe per IOV"""
    dict = {}
    for n,filename in enumerate(os.listdir(directory)):
        f = os.path.join(directory, filename)
        iov_dict, iov = get_iov_dict(f)
        if ONLY_NEW_FMT:
            if iov[0] < 368995: # first run number with new format
                print("old format, ignoring")
                continue
        layer_dict = get_layer_dict(iov_dict)
        df = get_df(layer_dict)
        if type(df) != None:
            dict[iov[0]] = df
        else:
            print(f"Problem with IOV {iov}, skipping.")
        if DEBUG: 
            print(df)
            print(df.columns)
            print("DEBUG mode. Will not process more IOVs")
            break
    return dict

def get_iov_dict(f):
    """Read a single file to a single dictionary"""
    print(f"Reading {f}")
    if os.path.isfile(f):
        dict, iov = ReadDbFile(f)
        return dict, iov
    else:
        raise FileNotFoundError

def get_layer_dict(iov_dict, layer="LI"):
    layer_dict = {}
    for hash in iov_dict.keys(): # module hash
        mod = MAP.loc[MAP['hash'] == int(hash)]
        if mod.empty:
            continue
        elif layer not in mod['name'].item():
            continue
        else:
            layer_dict[mod['name'].item()] = iov_dict[hash]
    return layer_dict

def get_df(dict):
    dict_format = {}
    for mod in dict.keys():
        for n,fe in enumerate(dict[mod]):
            dict_format[mod+"_FE_"+str(n)] = fe
    try:
        df = pd.DataFrame.from_dict(dict_format).transpose()
    except:
        print("Error reading to df.")
        return None
    df = df.rename(columns={0: "thr_norm", 1: "sig_norm", 2: "thr_gang", 3: "sig_gang"})
    df = df.rename(columns={i: "tot_"+str(i) for i in range(16)}) 
    return df

def dump_to_csv(d):
    for iov in d.keys():
        filename = "./csv/" + iov[0] + "_" + iov[1] + ".csv" 
        try:
            f = open(filename, "w")
            f.write(d[iov].to_csv())
            f.close()
        except:
            print(f"Problem writing {iov} to csv. Skipping.")

def hists(d, x):
    for iov in d.keys():
        d[iov].hist(column=x)
        fname = "./plots/hists/"+str(iov)+"_"+str(x)+".png"
        plt.savefig(fname)
        print(f"plot saved in {fname}")

def plot_means(d, x):
    a = []
    b = []
    print(a)
    print(b)
    for iov in d.keys():
        a.append(iov)
        b.append(d[iov].loc[:,x].mean())
    plt.scatter(a, b)
    fname = "./plots/"+str(x)+"_vs_iov.png"
    plt.show()
    plt.savefig(fname)
    print(f"plot saved in {fname}")

def plt_per_fe(d, x):
    all_x = []
    df = pd.DataFrame()
    iovs = []
    for iov in d.keys():
        iovs.append(iov)
    iovs.sort()
    for iov in iovs:
        df[iov] = d[iov].loc[:,x]
    sns.violinplot(data=df, split=True, inner="quart", showfliers=False)
    plt.xticks(rotation=45)
    fname = "./plots/"+str(x)+"_violin.png"
    plt.show()
    plt.savefig(fname)
    print(f"plot saved in {fname}")

if __name__ == "__main__":
    d = fill_dict()
    # hists(d, "thr_norm")
    # plot_means(d, "tot_14")
    plt_per_fe(d, "tot_14")
    # dump_to_csv(d)
